# Experiments Description

## 1 - Files

### 1.1 - Datasets
* `DB_features_6.txt`: struct_vector graph representation, each line has **six features** (attributes) of a specific graph
* `DB_graph2vec_128.txt`: graph2vec *json* file, each graph has **128 features**. Dims: 128; Epochs: 10; Learning Rate: 0.3
* `DB_embedding.txt`: converted graph2vec file to the same format as the **DB_features_6.txt**
* input_labels.Labels: files labels (whole dataset of graphs in *gexf* format is required). If the graph is power-law the label is 1, otherwise the label is 0.

### 1.2 - Scripts
* `read_json_graph2vec`: it reads **DB_graph2vec_128.txt** and converts it to the file to **DB_embedding.txt**
* degree_input: it reads a **graph file** (*gexf* format) and converts it to **DB_features_6.txt**
* `classifiers.py`: script of our experiments
* **classifier** options: svm, knn, gradient_boosting and random_forest 

### Usage
```bash
python classifiers.py <training_set> <testing_set> <whole_dataset> <classifier>
```

### Usage Examples: 
```bash
python classifiers.py DATAg1_rand.txt DATAg2_rand.txt DB_embedding.txt svm
```
```
python classifiers.py DATA61_rand.txt DATA62_rand.txt DB_features_6.txt svm
```

## 2 - Six Features (array)

### 2.1 - Properties
* maximum degree
* minimum degree
* density
* average degree
* number of vertices that have the maximum degree
* number of vertices that have the minimum degree