import pickle
from sklearn import preprocessing
from sklearn import model_selection
from sklearn.datasets import load_svmlight_file
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix
from sklearn import svm
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import GradientBoostingClassifier, RandomForestClassifier
from matplotlib import pyplot
from sklearn.neighbors import KNeighborsClassifier

train = './data/DATA61_rand.txt'
test = './data/DATA62_rand.txt'
file = './data/DB_features_6.txt'

def find_lognormals(y_test, y_pred, file, test_file):

    tf = test_file.readlines()
    fl = file.readlines()
    lognormals = 0

    for i, value in enumerate(y_test):
        if value != y_pred[i]:
            search = tf[i]
            search = search.rstrip()

            for j in range(2845, 3145):
                line = fl[j].rstrip()
                if search == line:
                    lognormals += 1


def get_results(y_train, y_test, y_train_pred, y_test_pred, file, test_file):

    acc = accuracy_score(y_test, y_test_pred)
    print("Accuracy in train: " + str(accuracy_score(y_train, y_train_pred)))
    print("Accuracy in test: " + str(acc))
    # print("Lognormals classified as power law: "
    #       + str(find_lognormals(y_test, y_test_pred, file, test_file)))
    print(classification_report(y_test, y_test_pred))
    print(confusion_matrix(y_test, y_test_pred))


def knn(x_train, y_train, x_test, y_test, file, test_file):

    param_grid = {'n_neighbors': [3, 5, 11]}
    grid_search = GridSearchCV(KNeighborsClassifier(), param_grid)
    grid_search.fit(x_train, y_train)

    neighbors = grid_search.best_estimator_.n_neighbors
    print('Best number of neighbors:' + str(neighbors))

    model = KNeighborsClassifier(n_neighbors=neighbors)
    model.fit(x_train, y_train)
    y_test_pred = model.predict(x_test)
    y_train_pred = model.predict(x_train)

    get_results(y_train, y_test,
                y_train_pred, y_test_pred, file, test_file)

    with open('./models/pkl_knn_model.pkl', 'wb') as file:
        pickle.dump(model, file)

    return model


def random_forest(x_train, y_train, x_test, y_test, file, test_file):
    param_grid = {'n_estimators': [50, 100, 200],
                  'max_depth': [3, 5, 9, 10]}

    grid_search = GridSearchCV(RandomForestClassifier(), param_grid)
    grid_search.fit(x_train, y_train)

    n_estimators = grid_search.best_estimator_.n_estimators
    max_depth = grid_search.best_estimator_.max_depth

    model = RandomForestClassifier(
        n_estimators=n_estimators, max_depth=max_depth)
    model.fit(x_train, y_train)
    y_train_pred = model.predict(x_train)
    y_test_pred = model.predict(x_test)

    get_results(y_train, y_test,
                y_train_pred, y_test_pred, file, test_file)

    with open('./models/pkl_rand_forest_model.pkl', 'wb') as file:
        pickle.dump(model, file)

    return model


def gradient_boosting(x_train, y_train, x_test, y_test, file, test_file):
    param_grid={
        'n_estimators':[50, 100, 200], 
        'learning_rate': [0.01,0.1,0.5], 
        'max_depth':[3,5,9,10]} 
        
    grid_search = GridSearchCV(GradientBoostingClassifier(), param_grid) 
    grid_search.fit(x_train, y_train) 
    n_estimators = grid_search.best_estimator_.n_estimators  
    learning_rate = grid_search.best_estimator_.learning_rate
    max_depth = grid_search.best_estimator_.max_depth 
    
    print('Best n_estimators:', n_estimators)
    print('Best learning_rate:', learning_rate)  
    print('Best max_depth:', max_depth)
    
    model = GradientBoostingClassifier(n_estimators=n_estimators, learning_rate=learning_rate, max_depth=max_depth) 
    model.fit(x_train, y_train)  
    
    y_train_pred = model.predict(x_train) 
    y_test_pred = model.predict(x_test)
    
    get_results(y_train, y_test,
                y_train_pred, y_test_pred, file, test_file) 

    with open('./models/pkl_grad_boosting_model.pkl', 'wb') as file: 
        pickle.dump(model, file) 
        
    return model


def svm(x_train, y_train, x_test, y_test, file, test_file):
    Cs = [1, 10, 100, 1000]
    gammas = [0.001, 0.01, 0.1, 1]
    param_grid = {'C': Cs, 'gamma' : gammas}
    grid_search = GridSearchCV(SVC(kernel='rbf'), param_grid, cv=5)
    grid_search.fit(x_train, y_train)

    c = grid_search.best_estimator_.C
    gamma = grid_search.best_estimator_.gamma

    print('Best C:', c)
    print('Best Gamma:', gamma)

    model = SVC(C=c, gamma=gamma, kernel='rbf',probability=True)
    model.fit(x_train, y_train)
    y_train_pred = model.predict(x_train)
    y_test_pred = model.predict(x_test)
  
    get_results(y_train, y_test,
                y_train_pred, y_test_pred, file, test_file)

    with open('./models/pkl_svg_model.pkl', 'wb') as file: 
        pickle.dump(model, file) 

    return model


fl = open(file)
train_file = open(train)
test_file = open(test)

print("Loading data...")
X_train, y_train = load_svmlight_file(train)
x_test, y_test = load_svmlight_file(test)

X_train_dense = X_train.toarray()
X_test_dense = x_test.toarray()

print("Normalizing...")
scaler = preprocessing.MinMaxScaler()
X_train_minmax = scaler.fit_transform(X_train_dense)
X_test_minmax = scaler.transform(X_test_dense)

knn(X_train_minmax, y_train, X_test_minmax, y_test, fl, test_file)
svm(X_train_minmax, y_train, X_test_minmax, y_test, fl, test_file)
gradient_boosting(X_train_minmax, y_train, X_test_minmax, y_test, fl, test_file)
random_forest(X_train_minmax, y_train, X_test_minmax, y_test, fl, test_file)

