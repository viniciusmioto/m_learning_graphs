# Real World Dataset

## Power-Law 37

* [amzn](https://snap.stanford.edu/data/amazon-meta.html)
* [amzn](https://snap.stanford.edu/data/com-Amazon.html)
* [caid](https://snap.stanford.edu/data/as-caida.html)
* [citt](https://snap.stanford.edu/data/cit-HepTh.html)
* [clab](https://snap.stanford.edu/data/ca-AstroPh.html)
* [emal](https://snap.stanford.edu/data/email-EuAll.html)
* [euss](https://subscription.packtpub.com/book/big_data_and_business_intelligence/9781783987405/9/ch09lvl1sec97/exploring-the-web-and-internet-domain-eurosis-web-mapping-study)
* [gloc](https://snap.stanford.edu/data/loc-Brightkite.html)
* [gnut](http://snap.stanford.edu/data/p2p-Gnutella08.html)
* [smcd](https://snap.stanford.edu/data/ego-Facebook.html)
* [scmd](http://snap.stanford.edu/data/soc-Epinions1.html)
* [scmd](https://snap.stanford.edu/data/soc-Slashdot0811.html)
* [smcd](https://snap.stanford.edu/data/twitch-social-networks.html)
* [webn](https://snap.stanford.edu/data/web-BerkStan.html)
* [webn](https://snap.stanford.edu/data/web-Google.html)
* [webn](https://snap.stanford.edu/data/web-NotreDame.html)
* [bion](http://graphdatasets.com/bio.php)

### Not-Power-Law 16
* [Enzymes](https://github.com/MLDroid/graph2vec_tf)
* [Brain networks](http://graphdatasets.com/bn.php)
* [eclg](http://graphdatasets.com/eco.php)
* [road](http://graphdatasets.com/road.php)
* [infr](http://graphdatasets.com/inf.php)
* [ecnm](http://graphdatasets.com/econ.php)