# Recognizing Power-law Graphs by Machine Learning Algorithms

## Undergraduate Research

I started this undergraduate research project by studying the paper "Recognizing Power-law Graphs by Machine Learning Algorithms using a Reduced Set of Structural Features" written by [Alane Marie de Lima](https://www.inf.ufpr.br/amlima/), [André Luis Vignatti](https://www.inf.ufpr.br/vignatti/) and [Murilo Vicente Gonçalves da Silva ](https://www.inf.ufpr.br/murilo/).

I have been studying some machine learning algorithms from [scikit-learn](https://scikit-learn.org/stable/) library as KNN, SVM, Gradient Boosting and Random Forests. I made some experiments with [NetworkX](https://networkx.org/) in order to understand how we can work with networks in computers. I am using [graph2vec](https://github.com/MLDroid/graph2vec_tf) framework for graph embedding.

### Advisors  
  * [André Luís Vignatti](https://www.inf.ufpr.br/vignatti/), PhD
  * [Alana Marie de Lima](https://www.inf.ufpr.br/amlima/), MSc 
