# Networks Data Sets Formats

## CSV Format
Comma Separated Values
Extension: \*.txt or \*csv
* Edgelist  
```csv
0 344  
0 345  
0 346  
1 48  
1 53  
...
```

There is an edge between the nodes 0 and 344, other edge between 0 and 345, and so on. We can use a third column to add weight to these edges.  

```csv
1 2 0.3  
3 4 0.1  
3 5 0.8  
...  
```
 
* Adjancy List (Adjlist)

```csv
1 2 5  
2 4 6  
3 1 4 7  
3 1 4 7  
8 9  
...  
```

The first value is the first node is called source node, and the subsequent values are nodes with edges linking to the source. Thus, the first row indicates that there is an edge between 1 and 2, other edge from 1 to 5. The second row indicates thes edges: (2 - 4), (2 - 6) and so on.

## GML Format
Graph Modeling Language

```python
graph
[  
    node  
    [  
        id A  
        label "Node A"
    ]  
    node  
    [  
        id B  
    ]  
    node  
    [  
        id C  
    ]  
    edge
    [
        source B
        target A
    ]
    edge
    [
        source C
        target A
        label "Edge C to A"
    ]
]  
```

This a simple and flexible format to assigning attributes to nodes and edges if you need to. It is easy to understand, in this case we have the nodes A, B and C. There are two edges: B to A and C to A. It is possible to set others attributes like hierarchic and directed graph.

## Pajek Net Format: .NET or .paj

```python
*Vertices 82670   | *arcs 
1 "entity"        | 4244 107
2 "thing"         | 4244 238
3 "anything"      | 4244 4292
4 "something"     | 4247 107
5 "nothing"       | 4248 1
6 "whole"         | 4248 54
...               | ...
```

All the numbers below 'Vetices' are nodes (or vertices), the strings after the numbers in the first row are basicly labels of the vertices. The numbers below 'arcs' are the edges with both the source and target node, it is possible to add a weight attribute by adding third value in the line. If the vertices don't have labels the file will look like this:

```python
*Vertices 9
1 2
1 9
2 9
2 3
2 8
3 8
3 4
...
```

## GraphML

This format uses tags (it looks like XML) to create nodes and edges, you can add the atributtes inside the tags. It is possible to asign colors to the nodes.

```html
<?xml version="1.0" encoding="UTF-8"?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns"  
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns
     http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
  <graph id="G" edgedefault="undirected">
    <node id="n0"/>
    <node id="n1"/>
    <data key="d0">blue</data>
    ...
    <edge source="n0" target="n1"/>
    ...
    </graph>
</graphml>
```

## GEXF

It is inspired by XML and it is used in Gephi software, which help us to visualize networks.

```html
<?xml version="1.0" encoding="UTF-8"?>
<gexf xmlns="http://www.gexf.net/1.2draft" version="1.2">
    <meta lastmodifieddate="2009-03-20">
        <creator>Gexf.net</creator>
        <description>A hello world! file</description>
    </meta>
    <graph mode="static" defaultedgetype="directed">
        <nodes>
            <node id="0" label="Hello" />
            <node id="1" label="Word" />
        </nodes>
        <edges>
            <edge id="0" source="0" target="1" />
        </edges>
    </graph>
</gexf>
```

# Visualization

* [Python using NetwokX](https://networkx.org/)
* [Gephi Sowftware](https://gephi.org/)
* [GraphVis](http://networkrepository.com/graphvis.php)
