# Networks Visualization

## GraphVis

It is probably the most simple tool to visualize graphs, but it is a good choice for analysis. There is no installation requirements, we can access [GraphVis](http://networkrepository.com/graphvis.php) on the Network Repository website, where we can find a lot of information about the network that we are analyzing, such as the number of vertices and edges, the maximum, minimum and average degree, diameter, mean distance and many others.


![svg](./Images/karate-graphvis.svg)  
Karate Clube Network in GraphVis

<br>

## Gephi

It is a software that can be installed in our computer. [Gephi](https://gephi.org/) is a famous tool to analyze and manipulate graphs, we can modify some attributes in order to get a better visualization. Let's see an example of modification of the Karate Clube network.

<img src="./Images/karate-gephi.svg" width="340">  
<img src="./Images/karate-gephi2.svg" width="400">  

Karate Clube Network in Gephi

## NetworkX (Python)

It is a great library to make experiments with graphs. But with large networks it can be very slow. Besides that, we can create visualizations and get statistical information by using the functions of the [NetworkX](https://networkx.org/) library.

<img src="./Images/karate-networkx.svg" width="400">  

Karate Clube in NetworkX plot with Matplotlib.