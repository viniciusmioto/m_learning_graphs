import networkx as nx

PATH = './physics/'

def write_label(type, index, is_power_law):
	line = type + str(index) + '.gexf ' + str(is_power_law) +'\n'
	f.write(line)

type = 'physics'
with open('graphs.Labels', 'a+') as f:
	for i in range(1, 4):
		graph = PATH + type + str(i) + '.txt'
		G = nx.read_adjlist(graph)
		file = './data/' + type + str(i) + '.gexf'
		nx.write_gexf(G, file)
		write_label(type, i, 1)

