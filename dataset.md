# Real World Dataset

* [CAIDA AS Relationships Datasets](https://snap.stanford.edu/data/as-caida.html)
* [Gnutella peer-to-peer networks](http://snap.stanford.edu/data/p2p-Gnutella08.html)
* [EuroSis web-mapping](https://subscription.packtpub.com/book/big_data_and_business_intelligence/9781783987405/9/ch09lvl1sec97/exploring-the-web-and-internet-domain-eurosis-web-mapping-study)
* [Enzymes](https://github.com/MLDroid/graph2vec_tf)
* [Brain networks](http://graphdatasets.com/bn.php)
* [Physics Collaboration Networks](https://snap.stanford.edu/data/ca-AstroPh.html)
* [Amazon Products Co-purchasing](https://snap.stanford.edu/data/amazon-meta.html)
* [EU Email Communication Network](https://snap.stanford.edu/data/email-EuAll.html)






